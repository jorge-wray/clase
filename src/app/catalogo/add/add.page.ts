import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CatalogoService } from '../catalogo.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.page.html',
  styleUrls: ['./add.page.scss'],
})
export class AddPage implements OnInit {
  formExample: FormGroup;
  constructor(
    private productService: CatalogoService,
    private router: Router
  ) { }

  ngOnInit() {
    this.formExample = new FormGroup({
      title: new FormControl(null, {
        updateOn: 'blur',
        validators:[Validators.required]
      }),
      description: new FormControl(null, {
        updateOn: 'blur',
        validators:[
          Validators.required,
          Validators.maxLength(20)
        ]
      }),
      id: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required]
      }),
      img: new FormControl(null, {
        validators: [Validators.required]
      })
    });
  }
  addFunction(){
    if(!this.formExample.valid){
      return;
    }
    console.log(this.formExample);
    this.productService.addProduct(
      this.formExample.value.title,
      this.formExample.value.id,
      this.formExample.value.img,
      this.formExample.value.description
    );
    this.router.navigate(['/catalogo']);
  }
}
