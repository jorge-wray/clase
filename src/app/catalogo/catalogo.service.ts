import { Injectable } from '@angular/core';
import { Producto } from './catalogo.model';

@Injectable({
  providedIn: 'root'
})
export class CatalogoService {
  private productos: Producto[] = [
    {
      id: "entry-1",
      title: "Entry 1",
      description: "Entry 1 description",
      img: "https://ar.zoetis.com/_locale-assets/mcm-portal-assets/publishingimages/especie/caninos_perro_img.png"
    },
    {
      id: "entry-2",
      title: "Entry 2",
      description: "Entry 2 description",
      img: "https://ar.zoetis.com/_locale-assets/mcm-portal-assets/publishingimages/especie/caninos_perro_img.png"
    }
  ];
  constructor() { }
  getAll(){
    return [...this.productos];
  }
  getProducto(catalogId: string){
    return {...this.productos.find(
      producto => {
        return catalogId === producto.id;
      }
    )};
  }
  deleteProduct(catalogId: string){
    this.productos = this.productos.filter(
      producto => {
        return producto.id !== catalogId;
      }
    );
  }
  addProduct(title: string, id: string, img: string, description: string){
    id = Math.random().toString();
    const newProduct = new Producto(
      img,
      title,
      description,
      id
    );
    this.productos.push(newProduct);
    console.log(this.productos);
  }
}
