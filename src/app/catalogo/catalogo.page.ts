import { Component, OnInit } from '@angular/core';
import { Producto } from './catalogo.model';
import { CatalogoService } from './catalogo.service';

@Component({
  selector: 'app-catalogo',
  templateUrl: './catalogo.page.html',
  styleUrls: ['./catalogo.page.scss'],
})
export class CatalogoPage implements OnInit {
  productos: Producto[];
  constructor(private catalogoServicio: CatalogoService) { }

  ngOnInit() {
    console.log("entro al init");
    this.productos = this.catalogoServicio.getAll();
  }
  ionViewWillEnter(){
    console.log("Entro al will enter");
    console.log("Entro al will enter");
    this.productos = this.catalogoServicio.getAll();
  }

}
