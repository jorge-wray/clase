export interface Producto{
  id: string;
  title: string;
  description: string;
  img:string;
}
export class Producto {
  constructor(
    public img: string,
    public title: string,
    public description: string,
    public id: string,
  ){}
}
