import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Producto } from '../catalogo.model';
import { CatalogoService } from '../catalogo.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit {
  producto: Producto;
  constructor(
    private activatedRoute: ActivatedRoute,
    private catalogoServicio: CatalogoService,
    private router: Router,
    private alertCtrl: AlertController
  ) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(
      paramMap => {
        if(!paramMap.has("catalogoId")){
          // No existe el parametro redirecciono
          return;
        }
        const productoId = paramMap.get("catalogoId");
        this.producto = this.catalogoServicio.getProducto(productoId);
      }
    );
  }
  delete(){
    this.alertCtrl.create({
      header: "Borrar",
      message: "Esta seguro que desea borrar este producto?",
      buttons: [
        {
          text: "Cancelar",
          role: 'cancel'
        },
        {
          text: 'Borrar',
          handler: () => {
            this.catalogoServicio.deleteProduct(this.producto.id);
            this.router.navigate(['/catalogo']);
          }
        }
      ]
    }).then(
      alertElement => {
        alertElement.present();
      }
    );

  }

}
